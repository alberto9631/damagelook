//
//  DamageController.swift
//  DamageLook
//
//  Created by ALBERTO GONZALEZ on 10/2/19.
//  Copyright © 2019 Alberto Gonzalez. All rights reserved.
//

import UIKit

class DamageController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrSections = [String]()
    var arrType = [String]()
    var arrSection = [String]()
    var isOrientation: Bool = true
    var sectionSelect = [String]()
    var section = String()
    var selectionLast = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        arrSections.append("Cofre")
        arrSections.append("Fascia Delantera")
        arrSections.append("Salpicadera")
        arrSections.append("Puerta delantera")
        arrSections.append("Puerta trasera")
        arrSections.append("Costado")
        arrSections.append("Fascia trasera")
        arrSections.append("Cajuela")
        arrSections.append("Faro")
        arrSections.append("Calavera")
        arrSections.append("Rin Trasero")
        arrSections.append("Rin Delantero")
        arrSections.append("Espejo")
        arrSections.append("Poste ")
        arrSections.append("Toldo")
        arrSections.append("Parabrisas")
        arrSections.append("Medallón")
        arrSections.append("Cristal Trasero")
        arrSections.append("Cristal Delantero")
        
        
        arrType.append("Izquierdo")
        arrType.append("Derecho")
        
        sectionSelect.append("0")
        sectionSelect.append("0")
        
        arrSection.append("Rayado")
        arrSection.append("Abollado")
        arrSection.append("Dañado")
        arrSection.append("Descarapelado")
        arrSection.append("Descuadrado")
        arrSection.append("Roto")
        arrSection.append("Estrellado")
        arrSection.append("Falta de componente")
        
        tableView.reloadData()
        
        // Do any additional setup after loading the view.
    }
    @objc
    func selectSection(sender: Any) {
        let btn = sender as! UIButton
        for (index, _) in sectionSelect.enumerated() {
           sectionSelect[index] = "0"
        }
        sectionSelect[btn.tag] = "1"
        section = arrType[btn.tag]
        tableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DamageController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        view.backgroundColor = .lightGray
        
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.text = arrType[section]
        view.addSubview(label)
        
        let btnSection = UIButton()
        btnSection.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50)
        btnSection.backgroundColor = .clear
        btnSection.tag = section
        btnSection.addTarget(self, action: #selector(selectSection(sender:)), for: .touchUpInside)
        view.addSubview(btnSection)
        
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isOrientation {
            return 0
        }
        return 50
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if isOrientation {
            return 1
        }
        return arrType.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOrientation {
            return arrSections.count
        }
        if sectionSelect[section] == "1" {
            return arrSection.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "CellText") as! CellText
        if isOrientation {
            cell.titleLabel.text = arrSections[indexPath.row]
        } else {
            cell.titleLabel.text = arrSection[indexPath.row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isOrientation {
        let newView = self.storyboard?.instantiateViewController(withIdentifier: "DamageController") as! DamageController
            newView.isOrientation = false
            newView.selectionLast = arrSections[indexPath.row]
        self.navigationController?.show(newView, sender: nil)
            return
        }
        let str = "\(selectionLast) \(section) \(arrSection[indexPath.row])"
        let alert = UIAlertController(title: "Seleccionado", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
        
    }
    
}

class CellText: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
}
